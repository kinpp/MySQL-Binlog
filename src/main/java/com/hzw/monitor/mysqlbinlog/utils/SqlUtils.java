package com.hzw.monitor.mysqlbinlog.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class SqlUtils {
	private static final Logger logger = LogManager.getLogger(SqlUtils.class);

	// private static final String TABLE_SCHEMA = "TABLE_SCHEMA";
	// private static final String TABLE_NAME = "TABLE_NAME";
	private static final String COLUMN_NAME = "COLUMN_NAME";
	private static final String ORDINAL_POSITION = "ORDINAL_POSITION";

	private static void getPrimaryKeyMapping(Connection conn, String database, String table,
			HashMap<String, ArrayList<String>> primaryKeysMapping) throws Exception {
		// 初始化为空，表示不存在主键
		ArrayList<String> pks = new ArrayList<String>();
		// 准备sql语句
		String sql = " SELECT k.column_name ";
		sql += " FROM information_schema.table_constraints t ";
		sql += " JOIN information_schema.key_column_usage k ";
		sql += " USING (constraint_name,table_schema,table_name) ";
		sql += " WHERE t.constraint_type='PRIMARY KEY' ";
		sql += " AND t.table_schema='" + database + "' ";
		sql += " AND t.table_name='" + table + "' ";
		// 开始查询
		Statement stmt = null;
		ResultSet resultSet = null;
		String columnName;
		try {
			stmt = conn.createStatement();
			resultSet = stmt.executeQuery(sql);
			while (null != resultSet && resultSet.next()) {
				columnName = resultSet.getString(COLUMN_NAME);
				pks.add(columnName);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			// 必须分开关闭
			// 关闭resultset
			if (null != resultSet) {
				try {
					resultSet.close();
				} catch (SQLException e) {
				}
			}
			// 关闭stmt
			if (null != stmt) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
		}
		LoggerUtils.debug(logger, "pks:[" + database + "][" + table + "] ---" + pks);
		primaryKeysMapping.put(StringUtils.union(database, table), pks);// 更新此数据
		// 返回
	}

	public static HashMap<String, String> getDatabaseTableColumnsMapping(String ip, int port, String username,
			String password, String database, String table, HashMap<String, ArrayList<String>> primaryKeysMapping)
					throws Exception {
		// 通过查询数据库来获取消息
		// LoggerUtils.debug(logger, "-----------------------------begin");
		HashMap<String, String> mappings = new HashMap<String, String>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		try {
			String url = "jdbc:mysql://" + ip + ":" + port + "/" + database;
			conn = DriverManager.getConnection(url, username, password);// 获取连接
			stmt = conn.createStatement();
			// 构造sql
			String sql = "select COLUMN_NAME, ORDINAL_POSITION ";
			sql += " from INFORMATION_SCHEMA.COLUMNS ";
			sql += "where TABLE_SCHEMA='" + database + "' and TABLE_NAME='" + table + "'";
			// LoggerUtils.debug(logger, "sql:" + sql);
			resultSet = stmt.executeQuery(sql);
			String columnName;
			int position;
			while (null != resultSet && resultSet.next()) {
				// 存在下一行数据
				// String tableSchema = resultSet.getString(TABLE_SCHEMA);
				// String tableSchema = resultSet.getString(TABLE_SCHEMA);
				columnName = resultSet.getString(COLUMN_NAME);
				position = resultSet.getInt(ORDINAL_POSITION);
				// LoggerUtils.debug(logger, "" + columnName + " " + position);
				mappings.put("" + (position - 1), columnName);
			}
			// 借着这个connection,获取主键关系
			getPrimaryKeyMapping(conn, database, table, primaryKeysMapping);
		} catch (Exception e) {
			LoggerUtils.error(logger,
					"connect to mysql to get meta mapping fail..." + ip + ":" + port + " " + e.toString());
			throw e;
		} finally {
			// 必须分开关闭
			// 关闭resultset
			if (null != resultSet) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// 关闭stmt
			if (null != stmt) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// 关闭conn
			if (null != conn) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		// LoggerUtils.debug(logger, "-----------------------------query mapping
		// end");
		return mappings;
	}

	public static boolean isAlterTableSql(String sql) {
		sql = sql.toUpperCase();
		return sql.startsWith("ALTER TABLE");
	}

	public static boolean isDropTableSql(String sql) {
		sql = sql.toUpperCase();
		return sql.startsWith("DROP TABLE");
	}

	public static String getAlterTableName(String sql) {
		String table = null;
		try {
			int firstPosition = -1;
			int secondPosition = -1;
			byte[] data = sql.getBytes();
			int length = data.length;
			for (int index = 0; index < length; index++) {
				if (0x60 == data[index]) {
					if (-1 == firstPosition) {
						firstPosition = index;
					} else if (-1 == secondPosition) {
						secondPosition = index;
						break;// 必须跳出
					}
				}
			}
			firstPosition++;
			table = new String(data, firstPosition, secondPosition - firstPosition);
		} catch (Exception e) {
			table = null;
		}
		return table;
	}

	public static String getDropTableName(String sql) {
		String table = null;
		try {
			int firstPosition = -1;
			int secondPosition = -1;
			byte[] data = sql.getBytes();
			int length = data.length;
			for (int index = 0; index < length; index++) {
				if (0x60 == data[index]) {
					if (-1 == firstPosition) {
						firstPosition = index;
					} else if (-1 == secondPosition) {
						secondPosition = index;
						break;// 必须跳出
					}
				}
			}
			firstPosition++;
			table = new String(data, firstPosition, secondPosition - firstPosition);
		} catch (Exception e) {
			table = null;
		}
		return table;
	}

	// public static void main(String[] args) {
	// HashMap<String, String> result =
	// getDatabaseTableColumnsMapping("172.x.x.x", 3306, "root", ".",
	// "skyeye",
	// "product");
	// LoggerUtils.debug(logger, result.toString());
	// }

	private static final String name = "com.mysql.jdbc.Driver";

	public static void init() {
		try {
			Class.forName(name);
		} catch (ClassNotFoundException e) {
			LoggerUtils.error(logger, e.toString());
			System.exit(-1);
		}
	}
}
