package com.hzw.monitor.mysqlbinlog.consumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import com.hzw.monitor.mysqlbinlog.snapshot.SnapShot;
import com.hzw.monitor.mysqlbinlog.utils.DataUtils;
import com.hzw.monitor.mysqlbinlog.utils.LoggerUtils;
import com.hzw.monitor.mysqlbinlog.utils.WriteResultUtils;

public class Consumer {

	private static final Logger logger = LogManager.getLogger(Consumer.class);
	private int index;
	private BlockingQueue<SnapShot> queue = null;
	private Thread thread = null;

	public Consumer(int id) {
		index = id;
		queue = new LinkedBlockingQueue<SnapShot>(1*1000);
	}

	// poll: 若队列为空，返回null。
	// remove:若队列为空，抛出NoSuchElementException异常。
	// take:若队列为空，发生阻塞，等待有元素。
	public Consumer start() {
		// 1)
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// 开始处理队列里的每一个数据
				SnapShot shot = null;
				while (true) {// 无限循环中,本线程永远不会退出
					// LoggerUtils.info(logger, "queue("+index+").size:
					// "+queue.size());
					try {// 尝试拿取数据
						shot = queue.take();
					} catch (InterruptedException e) {
						shot = null;
					}
					// 为空则继续
					if (null == shot) {
						LoggerUtils.info(logger, "no message from consume queue");
						continue;
					}
					// 成功拿到了一个SnapShot对象
					// 1)检测是否成功标志
					if (false == shot.getGlobalValid().get()) {
						shot.setWriteResult(WriteResultUtils.FAIL.ordinal());
						continue;
					}
					// 当前有效
					// type肯定是IO类型，否则不会加入到这里
					int result;
					if (shot.isAccepted()) {// 确实需要处理
						result = DataUtils.handleMulti(shot.getDb(), shot.getTable(), shot.getDatas())
								? WriteResultUtils.SUCCEED.ordinal() : WriteResultUtils.FAIL.ordinal();
					} else {
						result = WriteResultUtils.SUCCEED.ordinal();
					}
					// 将结果写回到result
					shot.setWriteResult(result);
					// 如果写入失败，设置全局失败标志
					if (WriteResultUtils.FAIL.ordinal() == result) {
						shot.getGlobalValid().set(false);
					}
				}
			}
		};
		// 2)
		thread = new Thread(runnable);
		// 3)
		thread.setDaemon(true);// 后台线程
		thread.setName("consumer_thread_" + index);
		thread.start();
		LoggerUtils.info(logger, thread.getName() + " started successfully...");
		return this;

	}

	// put---无空间会等待
	// add--- 满时,立即返回,会抛出异常
	// offer---满时,立即返回,不抛异常
	public void addSnapShot(SnapShot snapShot) {
		// 坚决不能阻塞，否则selector会挂住
		while (true == snapShot.getGlobalValid().get()) {
			// 如果还是有效的，则需要进入
			try {
				queue.add(snapShot);
				break;
			} catch (Exception e) {
				continue;
			}
		}
	}

}
